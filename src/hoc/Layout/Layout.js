import React, { Component } from 'react'
import GpWrapper from '../GpWrapper/GpWrapper'
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer'

class Layout extends Component {

    state = {
        showSideDrawer: false
    }

    hideNavigationHandler = () => {
        this.setState({
            showSideDrawer: false
        })
    }

    sideMenuToggleHandler = () => {
        this.setState((prevState) => {
            return { showSideDrawer: !prevState.showSideDrawer }
        })
    }

    render() {
        return (
            <GpWrapper>
                <Toolbar handleMenuButton={this.sideMenuToggleHandler}/>
                <SideDrawer closed={this.hideNavigationHandler} open={this.state.showSideDrawer}/>
                <main className={classes.Content}>
                    { this.props.children }
                </main>
            </GpWrapper>
        )
    }
}

export default Layout;