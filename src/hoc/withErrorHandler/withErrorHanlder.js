import React, { Component } from 'react'

import Modal from '../../components/UI/Modal/Modal'
import GpWrapper from '../GpWrapper/GpWrapper'


const withErrorHandler = ( WrapperComponent, axios ) => {
    return class extends Component {
        state = {
            error: null
        }
        componentWillMount () {
           this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({error: null})
                return req
            });

            this.resInterceptor = axios.interceptors.response.use(res => res, error => {
                this.setState({error: error})
            })
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptor)
            axios.interceptors.request.eject(this.resInterceptor)
        }

        errorConfirmedHanlder = () => {
            this.setState({error: null})
        }

        render () {
            return(
                <GpWrapper>
                    <Modal show={this.state.error} modalClosed={this.errorConfirmedHanlder}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrapperComponent {...this.props}/>
                </GpWrapper>
            )
        }
    }
}

export default withErrorHandler