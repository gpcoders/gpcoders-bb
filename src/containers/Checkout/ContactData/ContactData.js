import React, {Component} from 'react';
import Button from '../../../components/UI/Button/Button'
import gpClasses from './ContactData.css'
import gpAxios from '../../../axios-orders'
import Spinner from '../../../components/UI/Spinner/Spinner'
import GpInput from '../../../components/UI/Input/GpInput'

class ContactData extends Component {
    /**
     * @ { Setting up state }
     * @type {{orderForm: {name: {elementType: string, elementConfig: {type: string, placeholder: string}, value: string}, street: {elementType: string, elementConfig: {type: string, placeholder: string}, value: string}, pinCode: {elementType: string, elementConfig: {type: string, placeholder: string}, value: string}, country: {elementType: string, elementConfig: {type: string, placeholder: string}, value: string}, email: {elementType: string, elementConfig: {type: string, placeholder: string}, value: string}, deliveryMethod: {elementType: string, elementConfig: {options: *[]}, value: string}}, loading: boolean}}
     */
    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'You Name'
                },
                value: ''
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: ''
            },
            pinCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Postal Code'
                },
                value: ''
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'India'
                },
                value: ''
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your email'
                },
                value: ''
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {
                            value: 'fastest',
                            displayValue: 'Fastest'
                        },
                        {
                            value: 'cheapest',
                            displayValue: 'Cheapest'
                        }
                    ]
                },
                value: ''
            },
        },
        loading: false
    }

    orderHandler = (e) => {
        e.preventDefault()
        this.setState({loading: true})

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
        }
        gpAxios.post('/orders.json', order)
            .then(response => {
                this.setState({loading: false, purchasing: false})
                this.props.history.push('/')
            })
            .catch(error => {
                this.setState({loading: false})
            })
        console.log(this.props.ingredients)
    }

    handleInputChange = (event, inputIdentifire) => {
        /**
         * @ { clone the order form }
         * @type {{}}
         */
        const formDataUpdate = {
            ...this.state.orderForm
        }

        /**
         * @ { get the current field object (field where currently typing or changing) }
         * @type {{}}
         */
        const updatedOrderForm = {
            ...formDataUpdate[inputIdentifire]
        }

        /**
         * @ { get current value object and set value }
         */
        updatedOrderForm.value = event.target.value

        /**
         * @ { { Getting current object and pass the value that changed } }
         * @type {{}}
         */
        formDataUpdate[inputIdentifire] = updatedOrderForm;

        /**
         * @ { Resetting the state and got changed data }
         */
        this.setState({
            orderForm: formDataUpdate
        })
    }

    render() {

        /**
         * @ { Rendering form }
         * @type {Array}
         */
        const formElementsArray = []
        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id:key,
                config:this.state.orderForm[key]
            })
        }

        return (
            <div className={gpClasses.ContactData}>
                <h4>Enter you contact data</h4>
                {this.state.loading ? <Spinner/> : (
                    <form action="">
                        {/*
                            @ { Mapping array and creating form }
                        */}
                        {formElementsArray.map((formElement) => (
                            <GpInput
                                key={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                gpChanged={(event) => this.handleInputChange(event, formElement.id)}
                            />
                        ))}
                        <Button btnType="Success" clicked={this.orderHandler}>Order</Button>
                    </form>
                )}

            </div>
        );
    }
}

export default ContactData;
