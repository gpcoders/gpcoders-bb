import React, { Component } from 'react';
import Burger from '../../components/Burger/Burger'
/**
 * @Custom {Components}
 */
import GpWrapper from '../../hoc/GpWrapper/GpWrapper'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import gpAxios from '../../axios-orders'
import GpSpinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHanlder'


const INGREDIENT_PRICES = {
        salad: 0.5,
        cheese: 1,
        bacon: 3,
        meat: 5
}

class BurgerBuilder extends Component {

    /**
     * @type {{ingredients: {salad: number, bacon: number, cheese: number, meat: number}}}
     */
    state = {
        ingredients: null,
        totalPrice: 0,
        purchaseAble: false,
        purchasing: false,
        loading: false,
        error: null
    }

    componentDidMount () {
        gpAxios.get('https://gpcoders-bb.firebaseio.com/ingredients.json')
            .then(response => {
                this.setState({
                    ingredients: response.data
                })
            })
            .catch(error => {
                this.setState({
                    error: true
                })
            })
    }

    /**
     * @ { Enable / Disable order now button }
     * @param ingredients
     */
    updatePurchaseState = (ingredients) => {

        /**
         * {Checking ingredient state}
         * @type {any}
         */
        const sum = Object.keys(ingredients).map((keys) => {
            return ingredients[keys]
        }).reduce((sum, el) => {
            return sum + el
        }, 0)

        this.setState({purchaseAble: sum > 0})
    }

    addIngredientHandler = (type) => {

        const oldCount = this.state.ingredients[type]   // @Count { old state quantitiy }
        const updatedCounted = oldCount + 1 // @Count { adding 1 to old quantity }

        /**
         * @State { Checking current state }
         * @type {{}}
         */
        const updatedIngredients = {
            ...this.state.ingredients
        }

        /**
         * @Update { Updating value in old state }
         */
        updatedIngredients[type] = updatedCounted

        const priceAddition = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice + priceAddition

        /**
         * @State { updating ingredient and state }
         */
        this.setState({
            totalPrice: newPrice,
            ingredients: updatedIngredients
        })

        this.updatePurchaseState(updatedIngredients)
    }

    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type]   // @Count { old state quantitiy }

        if (oldCount <= 0) {
            return false;
        }

        const updatedCounted = oldCount - 1 // @Count { adding 1 to old quantity }

        /**
         * @State { Checking current state }
         * @type {{}}
         */
        const updatedIngredients = {
            ...this.state.ingredients
        }

        /**
         * @Update { Updating value in old state }
         */
        updatedIngredients[type] = updatedCounted

        const priceDeduction = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice - priceDeduction

        /**
         * @State { updating ingredient and state }
         */
        this.setState({
            totalPrice: newPrice,
            ingredients: updatedIngredients
        })

        this.updatePurchaseState(updatedIngredients)
    }

    purchaseHandler = () => {
        this.setState({purchasing: true})
    }


    purchaseCancelHandler = () => {
        this.setState({ purchasing: false })
    }

    purchaseContinueHandler = () => {



        const queryParams = []

        for (let i in this.state.ingredients) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]))
        }
        queryParams.push('price=' + this.state.totalPrice)
        const queryString = queryParams.join('&')

        this.props.history.push({
            pathname: '/checkout',
            search: '?' + queryString
        })
    }


    /**
     * @returns {*}
     */
    render() {
        /**
         * @Destructuring
         * @type {{}}
         */
        const disableInfo = {
            ...this.state.ingredients
        }

        /**
         * @{ Setting up true false according to value }
         */
        for (let key in disableInfo) {
            disableInfo[key] = disableInfo[key] <= 0
        }

        let orderSummary = null

        let burger = this.state.error ? <p style={{textAlign: 'center'}}>Ingredient can't be loaded!</p> : (
            <div style={{marginTop: "200px"}}>
                <GpSpinner />
            </div>
        )

        if (this.state.ingredients) {
            burger = (
                <GpWrapper>
                    <Burger ingredients={this.state.ingredients} />
                    <BuildControls
                        ingredientAdded={this.addIngredientHandler}
                        ingredientRemoved={this.removeIngredientHandler}
                        disabled={disableInfo}
                        price={this.state.totalPrice}
                        updatePurchaseState={this.state.purchaseAble}
                        ordered={this.purchaseHandler}
                    />
                </GpWrapper>
            )

            orderSummary = <OrderSummary
                ingredients={this.state.ingredients}
                modalClosed={this.purchaseCancelHandler}
                purchaseContinue={this.purchaseContinueHandler}
                totalPrice={this.state.totalPrice.toFixed(2)}
                priceObject={INGREDIENT_PRICES}
            />
        }

        if (this.state.loading) {
            orderSummary = (
                <div style={{marginTop: "200px"}}>
                    <GpSpinner />
                </div>
            )
        }



        return (
            <GpWrapper>
                <Modal
                    show={this.state.purchasing}
                    modalClosed={this.purchaseCancelHandler}
                    >
                    {orderSummary}
                </Modal>
                {burger}
            </GpWrapper>
        );
    }
}

export default withErrorHandler(BurgerBuilder, gpAxios);
