import React from 'react'
import gpClasses from './Burger.css'
import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";

/**
 * @param props
 * @returns {*}
 */
const burger = (props) => {
    /**
     * @type {any[]}
     */
    let transformedIngredients = Object.keys(props.ingredients)
        .map((ingKeys) => {
            return [...Array(props.ingredients[ingKeys])].map((_, i) => {
                return <BurgerIngredient key={ingKeys + i} type={ingKeys}/>
            })
        }).reduce((arr, el) => {
            return arr.concat(el)
        }, [])

    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients</p>
    }

    /**
     * @Return { jsx }
     */
    return(
        <div className={gpClasses.Burger}>
            <BurgerIngredient type="bread-top"/>
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    )
}

export default burger;
