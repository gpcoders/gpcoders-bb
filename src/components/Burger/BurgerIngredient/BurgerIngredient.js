import React, { Component } from 'react'
import PropTypes from 'prop-types'
import gpClasses from './BurgerIngredient.css'

/**
 * @param props
 * @returns {null}
 */
class BurgerIngredient extends Component {

    render() {

        /**
         * @type {null}
         */
        let ingredient = null

        /**
         * @Switch {Checking Ingredient type}
         */
        switch (this.props.type) {
            case ('bread-bottom'):
                ingredient = <div className={gpClasses.BreadBottom}></div>
                break
            case ('bread-top'):
                ingredient = (
                    <div className={gpClasses.BreadTop}>
                        <div className={gpClasses.Seeds1}></div>
                        <div className={gpClasses.Seeds2}></div>
                    </div>
                )
                break
            case ('meat'):
                ingredient = <div className={gpClasses.Meat}></div>
                break
            case ('cheese'):
                ingredient = <div className={gpClasses.Cheese}></div>
                break
            case ('salad'):
                ingredient = <div className={gpClasses.Salad}></div>
                break
            case ('bacon'):
                ingredient = <div className={gpClasses.Bacon}></div>
                break
            default:
                ingredient = null
        }

        /**
         * @Return {Ingredient}
         */
        return ingredient
    }
}

BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired
}


export default BurgerIngredient