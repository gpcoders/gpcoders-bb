import React from 'react'
import GpWrapper from '../../../hoc/GpWrapper/GpWrapper'
import GpButton from '../../UI/Button/Button'
import gpClasses from './OrderSummary.css'

const orderSummary = (props) => {

    const ingredientsSummary = Object.keys(props.ingredients).map((ingKey) => {
        return (
            <li className={gpClasses.orderSumarryList} key={ingKey}>
                <span style={{textTransform: "uppercase"}}>{ingKey} :</span>
                <span>{props.ingredients[ingKey]}</span> -
                <span>{(props.ingredients[ingKey] * props.priceObject[ingKey]).toFixed(2)}</span>
            </li>
        )
    })

    return (
        <GpWrapper>
            <h3>Your Order</h3>
            <p>A Delicious burger with the following ingredients:</p>
            <ul>
                {ingredientsSummary}
            </ul>
            <p><strong>Total: {props.totalPrice}</strong></p>
            <p>Countiue to checkout</p>
            <GpButton btnType="Danger" clicked={props.modalClosed}>Cancel</GpButton>
            <GpButton btnType="Success" clicked={props.purchaseContinue}>Countiue</GpButton>
        </GpWrapper>
    )
}

export default orderSummary