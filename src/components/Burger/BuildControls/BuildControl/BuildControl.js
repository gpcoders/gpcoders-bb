import React from 'react'

import gpClasses from './BuildControl.css'

const buildControl = (props) => (
    <div className={gpClasses.BuildControl}>
        <div className={gpClasses.Label} style={{color: 'white'}}>{props.label}</div>
        <button className={gpClasses.Less} onClick={props.remove} disabled={props.disabled}>Less</button>
        <button className={gpClasses.More} onClick={props.added}>More</button>
    </div>
);

export default buildControl