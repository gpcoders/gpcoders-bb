import React from 'react'
import gpClasses from './BuildControl.css'
import BuildControl from './BuildControl/BuildControl'

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
]

const buildControls = (props) => (
    <div className={gpClasses.BuildControls}>
        <p style={{color: '#fff'}}>Current Price: <strong>{props.price.toFixed(2)} </strong></p>
        {controls.map((ctrl) => (
            <BuildControl
                key={ctrl.label}
                label={ctrl.label}
                added={() => props.ingredientAdded(ctrl.type)}
                remove={() => props.ingredientRemoved(ctrl.type)}
                disabled={props.disabled[ctrl.type]}
            />
        ))}
        <br />
        <button className={gpClasses.OrderButton} disabled={!props.updatePurchaseState} onClick={props.ordered}>Order Now</button>
    </div>
)

export default buildControls