import React from 'react'
import burgerLogo from '../../assets/images/burger-logo.png'
import gpClasses from './Logo.css'
const logo = (props) => (
  <div className={gpClasses.Logo}>
      <img src={burgerLogo} alt="gpCoders Logo"/>
  </div>
);

export default logo