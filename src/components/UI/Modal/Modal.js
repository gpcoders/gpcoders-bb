import React, { Component } from 'react'
import gpClasses from './Modal.css'
import GpWrapper from '../../../hoc/GpWrapper/GpWrapper'
import BackDrop from '../Backdrop/Backdrop'

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children
    }

    render() {
        return (
            <GpWrapper>
                <BackDrop show={this.props.show} clicked={this.props.modalClosed}/>
                <div
                    className={gpClasses.Modal}
                    style={{
                        transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: this.props.show ? '1' : '0'
                    }}
                >
                    {this.props.children}
                </div>
            </GpWrapper>
        );
    }
}

Modal.propTypes = {};

export default Modal;