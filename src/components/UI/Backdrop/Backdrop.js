import React from 'react'
import gpClasses from './Backdrop.css'

const backdrop = (props) => (
    props.show ? <div className={gpClasses.Backdrop} onClick={props.clicked}></div> : null
)

export default backdrop