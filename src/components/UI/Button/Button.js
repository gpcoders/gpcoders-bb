import React from 'react'
import gpClasses from './Button.css'

/**
 * @Button Actions
 * @param props
 * @returns {*}
 */
const button = (props) => (
    <button className={[gpClasses.Button, gpClasses[props.btnType]].join(' ')} onClick={props.clicked}>{props.children}</button>
)

export default button