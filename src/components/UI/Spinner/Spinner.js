import React from 'react'
import gpClasses from './Spinner.css'

const spinner = () => (
    <div className={gpClasses.loader}>Loading...</div>
)

export default spinner