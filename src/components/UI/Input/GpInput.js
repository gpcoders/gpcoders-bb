import React from 'react';
import gpClasses from './GpInput.css'

const gpInput = ( props ) => {
    /**
     * @type {null}
     */
    let inputElements = null

    /**
     * @ { Setting up form fields according to type }
     */
    switch ( props.elementType ) {
        case ( 'input' ):
            inputElements = <input
                className={gpClasses.gpInputElements}
                {...props.elementConfig}
                value={props.value}
                onChange={props.gpChanged}
            />
            break
        case ( 'textarea' ):
            inputElements = <textarea
                className={gpClasses.gpInputElements}
                {...props.elementConfig}
                value={props.value}
                onChange={props.gpChanged}
            />
            break
        case ( 'select' ):
            inputElements = (
                <select
                    className={gpClasses.gpInputElements}
                    value={props.value}
                    onChange={props.gpChanged}
                >
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>{option.displayValue}</option>
                    ))}
                </select>
            )
            break
        default:
            inputElements =
                <input
                    className={gpClasses.gpInputElements}
                    {...props.elementConfig}
                    value={props.value}
                    onChange={props.gpChanged}
                />
            break
    }

    return (
        <div className={gpClasses.gpInput}>
            <label className={gpClasses.gpLabel}>{ props.label }</label>
            {inputElements}
        </div>
    );
}

export default gpInput;
