import React from 'react'
import gpClasses from './NavigationItem.css'

const navigationItem = (props) => (
    <li className={gpClasses.NavigationItem}>
        <a
            href={props.link}
            className={props.active ? gpClasses.active : null }>
            {props.children}
            </a>
    </li>
);

export default navigationItem