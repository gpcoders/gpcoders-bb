import React from 'react'
import gpClasses from './Toolbar.css'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle'


const toolbar = (props) => (
    <header className={gpClasses.Toolbar}>
        <DrawerToggle handleMenuButton={props.handleMenuButton} />
        <div className={gpClasses.Logo}>
            <Logo />
        </div>
        <nav className={gpClasses.DesktopOnly}>
            <NavigationItems/>
        </nav>
    </header>
)

export default toolbar