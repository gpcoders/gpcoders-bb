import React from 'react'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import gpClasses from './SideDrawer.css'
import BackDrop from '../../UI/Backdrop/Backdrop'
import GpWrapper from '../../../hoc/GpWrapper/GpWrapper'

const sideDrawer = (props) => {

    let attachedClass = [gpClasses.SideDrawer, gpClasses.Close]

    if (props.open) {
        attachedClass = [gpClasses.SideDrawer, gpClasses.Open]
    }

    return(
        <GpWrapper>
            <BackDrop show={props.open} clicked={props.closed} />
            <div className={attachedClass.join(' ')}>
                <div className={gpClasses.Logo}>
                    <Logo/>
                </div>
                <nav>
                    <NavigationItems/>
                </nav>
            </div>
        </GpWrapper>
    );
}

export default sideDrawer