import React from 'react'
import gpClasses from './DrawerToggle.css'

const drawerToggle = (props) => (
    <div className={gpClasses.DrawerToggle} onClick={props.handleMenuButton}>
        <div></div>
        <div></div>
        <div></div>
    </div>
)

export default drawerToggle