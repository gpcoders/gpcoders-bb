import gpAxios from 'axios'

const gpInstance = gpAxios.create({
    baseURL: 'https://gpcoders-bb.firebaseio.com/'
})

export default gpInstance